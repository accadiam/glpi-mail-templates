#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import config as cfg
from premailer import transform as inline_css
from jinja2 import Environment, FileSystemLoader, select_autoescape
env = Environment(
    loader=FileSystemLoader('./j2'),
    autoescape=select_autoescape(['html'])
)

def mk_template( j2_file ):
    print(f"Création modèle de notification à partir de : {j2_file}")
    template = env.get_template(j2_file)


    mail = inline_css(template.render(
                                        warning=cfg.WARNING,
                                        footer=cfg.FOOTER,
                                        ticket_num_link=cfg.TICKET_NUM_LINK
                                    )
                    )

    with open( cfg.TGT_DIR + '/' + j2_file[:-3], "w") as text_file:
        print(mail, file=text_file)

    return

mk_template('resolution.html.j2')
mk_template('nouveau_ticket.html.j2')
mk_template('nouveau_suivi.html.j2')
mk_template('attribution.html.j2')
