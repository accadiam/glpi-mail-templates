# Outil de création de modèles de mail pour GLPI

**Statut :** beta

Crée les modèles de mail dans `templates` à partir des modèles jinja2 du dossier `jinja2`.

## Utilisation

1. Créer un venv et l'activer

```shell
python3 -m venv venv
source venv/bin/activate
```

2. Installer les dépendances

```shell
pip3 install -r requirements.txt
```

3. Modifier `config.py`

4. Executer

```shell
./mk_glpi_templates.py
```

Les modèles de notification sont créés dans le dossier `templates`.

5. Copier le code html des modèles dans l'interface GLPI (il ne faut pas copier le fichier entier ; seulement la partie indiquée). Ajouter également le CSS présent dans le dossier `templates` dans le champ CSS de GLPI.

## Liste des modèles

* Nouveau ticket
* Nouveau suivi
* Attribution d'un ticket
* Ticket résolu
