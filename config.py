# config file for mk_glpi_templates.py

TGT_DIR = './templates'
#WARNING = '''Vous voyez actuellement un prototype de nouvelle notification. Si vous avez des remarques, merci de les envoyer à <a href="mailto:maxime.accadia@univ-grenoble-alpes.fr">maxime.accadia@univ-grenoble-alpes.fr'''
WARNING = ""
#FOOTER = '''<p>Si vous êtes personnel UGA, vous pouvez aussi suivre l'évolution de votre ticket sur le site <a title="help.univ-grenoble-alpes.fr" href="https://help.univ-grenoble-alpes.fr/index.php?redirect=ticket_134442" target="_blank" el="noopener">help.univ-grenoble-alpes.fr</a> en utilisant vos identifiants AGALAN.</p>
#<p>Bonne journée :)</p>
#<p>Équipe de la DGDSI de l'UGA</p>
#<p>Assistance au <a href="tel:+33457421313">04 57 42 13 13</a> et <a href="mailto:help@univ-grenoble-alpes.fr">help@univ-grenoble-alpes.fr</a></p>'''
FOOTER = '''<p>Si vous êtes personnel UGA, vous pouvez suivre l'évolution de votre ticket sur le site <a title="help.univ-grenoble-alpes.fr" href="##ticket.url##" target="_blank" rel="noopener">help.univ-grenoble-alpes.fr</a>.</p>
<p><a title="Assistance" href="https://services-numeriques-personnels.univ-grenoble-alpes.fr/bandeau-haut-menu-boutons/de-l-aide-/assistance-informatique/" target="_blank" rel="noopener">En savoir plus sur l'assistance informatique de l'UGA</a></p>'''
TICKET_NUM_LINK='''<a title="ULR ticket" href="##ticket.url##" target="_blank" rel="noopener">##ticket.id##</a>'''
